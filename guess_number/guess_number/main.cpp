#include <iostream>
#include "NumberRandomizer.h"

using namespace std;

int main()
{
	char c;
	cout << "Enter a number" << endl;
	NumberRandomizer* random = new NumberRandomizer();

	for (;;)
	{
		cin >> c;
		if (c > '9' || c < '0')
		{
			cout << "Not a number" << endl;
		}
		else
		{
			bool result = random->checkNumber((int)(c - '0'));
			cout << (result ? "You WIN!!!" : "You lose, try again") << endl;
			if (result)
				random->changeNumber();
		}
	}
}