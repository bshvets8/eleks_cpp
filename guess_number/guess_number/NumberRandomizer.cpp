#include "NumberRandomizer.h"
#include <cstdlib>



NumberRandomizer::NumberRandomizer()
{
	changeNumber();
}

bool NumberRandomizer::checkNumber(int n)
{
	return number == n;
}

void NumberRandomizer::changeNumber()
{
	number = rand() % 9 + 1;
}


NumberRandomizer::~NumberRandomizer()
{
}
